package com.io.bodega.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.io.bodega.model.Bodega;
import com.io.bodega.service.BodegaService;

@RestController
@RequestMapping("/bodega")
public class BodegaController {
	
	@Autowired
	private BodegaService bodegaService;
	
	@GetMapping
	public @ResponseBody List<Bodega> findAll(){
		return bodegaService.findAll();		
	}
	
	@GetMapping("/{id}")
	public @ResponseBody Bodega findById(@PathVariable("id") Integer id ){
		return bodegaService.findById(id);
	}
	
	@PostMapping
	public ResponseEntity<Object> save(@RequestBody Bodega bodega){
		Object nuevaBodega = bodegaService.save(bodega);	
		return new ResponseEntity<Object>(nuevaBodega,HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public @ResponseBody Bodega update(@PathVariable("id") Integer id ,@RequestBody Bodega bodega){
		return bodegaService.update(bodega, id);
	}

}
