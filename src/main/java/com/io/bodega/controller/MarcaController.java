package com.io.bodega.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.io.bodega.model.Marca;
import com.io.bodega.service.MarcaService;

@RestController
@RequestMapping("/marca")
public class MarcaController {
	
	@Autowired
	private MarcaService marcaService;
	
	@GetMapping
	public @ResponseBody List<Marca> findAll(){
		return marcaService.findAll();		
	}
	
	@GetMapping("/{id}")
	public @ResponseBody Marca findById(@PathVariable("id") Integer id ){
		return marcaService.findById(id);
	}
	
	@PostMapping
	public ResponseEntity<Object> save(@RequestBody Marca presentacion){
		Object nuevaMarca = marcaService.save(presentacion);	
		return new ResponseEntity<Object>(nuevaMarca,HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public @ResponseBody Marca update(@PathVariable("id") Integer id ,@RequestBody Marca marca){
		return marcaService.update(marca, id);
	}

}
