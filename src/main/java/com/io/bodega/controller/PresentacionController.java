package com.io.bodega.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.io.bodega.model.Presentacion;
import com.io.bodega.service.PresentacionService;

@RestController
@RequestMapping("/presentacion")
public class PresentacionController {
	
	@Autowired
	private PresentacionService presentacionService;
	
	@GetMapping
	public @ResponseBody List<Presentacion> findAll(){
		return presentacionService.findAll();		
	}
	
	@GetMapping("/{id}")
	public @ResponseBody Presentacion findById(@PathVariable("id") Integer id ){
		return presentacionService.findById(id);
	}
	
	@PostMapping
	public ResponseEntity<Object> save(@RequestBody Presentacion presentacion){
		Object nuevoProducto = presentacionService.save(presentacion);	
		return new ResponseEntity<Object>(nuevoProducto,HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public @ResponseBody Presentacion update(@PathVariable("id") Integer id ,@RequestBody Presentacion presentacion){
		return presentacionService.update(presentacion, id);
	}

}
