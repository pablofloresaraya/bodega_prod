package com.io.bodega.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.io.bodega.model.Producto;
import com.io.bodega.service.ProductoService;

@RestController
@RequestMapping("/producto")
public class ProductoController {
	
	@Autowired
	private ProductoService productoService;
	
	@GetMapping
	public @ResponseBody List<Producto> findAll(){
		return productoService.findAll();		
	}
	
	@GetMapping("/{id}")
	public @ResponseBody Producto findById(@PathVariable("id") Integer id ){
		return productoService.findById(id);
	}
	
	@PostMapping
	public ResponseEntity<Object> save(@RequestBody Producto producto){
		Object nuevoProducto = productoService.save(producto);	
		return new ResponseEntity<Object>(nuevoProducto,HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public @ResponseBody Producto update(@PathVariable("id") Integer id ,@RequestBody Producto producto){
		return productoService.update(producto, id);
	}

}
