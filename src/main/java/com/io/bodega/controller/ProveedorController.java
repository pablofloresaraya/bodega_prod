package com.io.bodega.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.io.bodega.model.Proveedor;
import com.io.bodega.service.ProveedorService;

@RestController
@RequestMapping("/proveedor")
public class ProveedorController {
	
	@Autowired
	private ProveedorService proveedorService;
	
	@GetMapping
	public @ResponseBody List<Proveedor> findAll(){
		return proveedorService.findAll();		
	}
	
	@GetMapping("/{id}")
	public @ResponseBody Proveedor findById(@PathVariable("id") Integer id ){
		return proveedorService.findById(id);
	}
	
	@PostMapping
	public ResponseEntity<Object> save(@RequestBody Proveedor proveedor){
		Object nuevoProveedor = proveedorService.save(proveedor);	
		return new ResponseEntity<Object>(nuevoProveedor,HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public @ResponseBody Proveedor update(@PathVariable("id") Integer id ,@RequestBody Proveedor proveedor){
		return proveedorService.update(proveedor, id);
	}

}
