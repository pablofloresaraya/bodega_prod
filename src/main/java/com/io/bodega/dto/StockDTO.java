package com.io.bodega.dto;

import com.io.bodega.model.Bodega;
import com.io.bodega.model.Producto;

public class StockDTO {
	
	private Integer cantidad;
	private Bodega bodega;
	private Producto producto;
	
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
    
    public Bodega getBodega() {
        return bodega;
    }

    public void setBodega(Bodega bodega) {
        this.bodega = bodega;
    }
    
    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

}
