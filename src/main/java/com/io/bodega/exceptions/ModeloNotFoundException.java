package com.io.bodega.exceptions;

public class ModeloNotFoundException extends RuntimeException {
	
    public ModeloNotFoundException(String message) {
        super(message);
    }

}
