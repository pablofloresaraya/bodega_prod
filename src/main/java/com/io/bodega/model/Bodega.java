package com.io.bodega.model;

import javax.persistence.*;

@Entity
@Table(name="bodega")
@SequenceGenerator(
	name="BodegaSeq",
	sequenceName = "BODEGA_SEQ",
	initialValue = 1, 
	allocationSize = 10
)
public class Bodega {
	
	@Id
	@Column(name = "id_bodega")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BodegaSeq")
	private Integer id_bodega;
	
	@Column(name = "descripcion")
	private String descripcion;
	
    public Integer getIdBodega() {
        return id_bodega;
    }

    public void setIdBodega(Integer id_bodega) {
        this.id_bodega = id_bodega;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
