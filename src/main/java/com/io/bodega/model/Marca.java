package com.io.bodega.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="marca")
@SequenceGenerator(
	name="MarcaSeq",
	sequenceName = "MARCA_SEQ",
	initialValue = 1, 
	allocationSize = 10
)
public class Marca {
	
	@Id
	@Column(name = "id_marca")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MarcaSeq")
	private Integer id_marca;
	
	@Column(name = "descripcion")
	private String descripcion;
	
    public Integer getIdMarca() {
        return id_marca;
    }

    public void setIdMarca(Integer id_marca) {
        this.id_marca = id_marca;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
