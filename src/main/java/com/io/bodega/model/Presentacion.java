package com.io.bodega.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="presentacion")
@SequenceGenerator(
	name="PresentacionSeq",
	sequenceName = "PRESENTACION_SEQ",
	initialValue = 1, 
	allocationSize = 10
)
public class Presentacion {
	
	@Id
	@Column(name = "id_presentacion")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PresentacionSeq")
	private Integer id_presentacion;
	
	@Column(name = "descripcion")
	private String descripcion;
		
    public Integer getIdPresentacion() {
        return id_presentacion;
    }

    public void setIdPresentacion(Integer id_presentacion) {
        this.id_presentacion = id_presentacion;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
