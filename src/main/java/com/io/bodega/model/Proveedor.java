package com.io.bodega.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="proveedor")
@SequenceGenerator(
	name="ProveedorSeq",
	sequenceName = "PROVEEDOR_SEQ",
	initialValue = 1, 
	allocationSize = 10
)
public class Proveedor {
	
	@Id
	@Column(name = "id_proveedor")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ProveedorSeq")
	private Integer id_proveedor;
	
	@Column(name = "descripcion")
	private String descripcion;
		
    public Integer getIdProveedor() {
        return id_proveedor;
    }

    public void setIdProveedor(Integer id_proveedor) {
        this.id_proveedor = id_proveedor;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
