package com.io.bodega.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="stock")
@IdClass(StockPK.class)
public class Stock implements Serializable {
	
	@Id
	private Bodega id_bodega;
	
	@Id
	private Producto id_producto;
	
	@Column(name = "cantidad")
	private Integer cantidad;

    public Producto getProducto() {
        return id_producto;
    }

    public void setProducto(Producto id_producto) {
        this.id_producto = id_producto;
    }
    
    public Bodega getBodega() {
        return id_bodega;
    }

    public void setBodega(Bodega id_bodega) {
        this.id_bodega = id_bodega;
    }
    
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

}
