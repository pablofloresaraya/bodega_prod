package com.io.bodega.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class StockPK implements Serializable {
	
	@ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "id_producto", nullable = false)
    private Producto id_producto;
	
	@ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "id_bodega", nullable = false)
    private Bodega id_bodega;
	
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id_producto == null) ? 0 : id_producto.hashCode());
        result = prime * result + ((id_bodega == null) ? 0 : id_bodega.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StockPK other = (StockPK) obj;
        if (id_producto == null) {
            if (other.id_producto != null)
                return false;
        } else if (!id_producto.equals(other.id_producto))
            return false;
        if (id_bodega == null) {
            if (other.id_bodega != null)
                return false;
        } else if (!id_bodega.equals(other.id_bodega))
            return false;
        return true;
    }

}
