package com.io.bodega.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.io.bodega.model.Bodega;

public interface BodegaRepository extends JpaRepository<Bodega, Integer> {

}
