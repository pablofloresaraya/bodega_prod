package com.io.bodega.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.io.bodega.model.Presentacion;

public interface PresentacionRepository extends JpaRepository<Presentacion, Integer> {

}
