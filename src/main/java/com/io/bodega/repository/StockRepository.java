package com.io.bodega.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.io.bodega.model.Bodega;
import com.io.bodega.model.Producto;
import com.io.bodega.model.Stock;

@Repository
public interface StockRepository extends JpaRepository<Stock, Integer>{
	
	@Query(value = "insert into stock (cantidad, id_bodega, id_producto) values (:cantidad, :id_bodega, :id_producto)", nativeQuery = true)
	Stock saveStock(Integer cantidad, Integer id_bodega, Integer id_producto);
	
}
