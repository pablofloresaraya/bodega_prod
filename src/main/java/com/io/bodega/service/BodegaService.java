package com.io.bodega.service;

import java.util.List;

import com.io.bodega.model.Bodega;


public interface BodegaService {
	
	List <Bodega> findAll();
	
	Bodega findById(Integer id);
	
	Bodega save(Bodega bodega);
	
	Bodega update(Bodega bodega, Integer id);

}
