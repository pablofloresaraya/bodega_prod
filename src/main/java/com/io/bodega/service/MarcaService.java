package com.io.bodega.service;

import java.util.List;

import com.io.bodega.model.Marca;

public interface MarcaService {
	
	List <Marca> findAll();
	
	Marca findById(Integer id);
	
	Marca save(Marca marca);
	
	Marca update(Marca marca, Integer id);

}
