package com.io.bodega.service;

import java.util.List;

import com.io.bodega.model.Presentacion;

public interface PresentacionService {
	
	List <Presentacion> findAll();
	
	Presentacion findById(Integer id);
	
	Presentacion save(Presentacion presentacion);
	
	Presentacion update(Presentacion presentacion, Integer id);

}
