package com.io.bodega.service;

import java.util.List;

import com.io.bodega.model.Producto;

public interface ProductoService {
	
	List <Producto> findAll();
	
	Producto findById(Integer id);
	
	Producto save(Producto producto);
	
	Producto update(Producto producto, Integer id);
	
}
