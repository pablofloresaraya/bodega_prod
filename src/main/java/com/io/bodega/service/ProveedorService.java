package com.io.bodega.service;

import java.util.List;

import com.io.bodega.model.Proveedor;

public interface ProveedorService {
	
	List <Proveedor> findAll();
	
	Proveedor findById(Integer id);
	
	Proveedor save(Proveedor proveedor);
	
	Proveedor update(Proveedor proveedor, Integer id);

}
