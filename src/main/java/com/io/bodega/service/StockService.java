package com.io.bodega.service;

import java.util.List;

import com.io.bodega.model.Stock;

public interface StockService {
	
	List <Stock> findAll();
	
	Stock save(Stock stock);
	
	//Stock saveStock(Integer cantidad, Integer id_bodega, Integer id_producto);

}
