package com.io.bodega.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.io.bodega.exceptions.ModeloNotFoundException;
import com.io.bodega.model.Bodega;
import com.io.bodega.repository.BodegaRepository;
import com.io.bodega.service.BodegaService;

@Service
public class BodegaServiceImpl implements BodegaService {
	
	@Autowired
	private BodegaRepository bodegaRepository;
	
	@Override
	public List<Bodega> findAll() {
		return bodegaRepository.findAll();
	}

	@Override
	public Bodega findById(Integer id) {
		Optional<Bodega> bodegaO = bodegaRepository.findById(id);		
		if(bodegaO.isPresent()) {
			return bodegaO.get();
		}else {
			throw new ModeloNotFoundException("La bodega no existe");
		}
	}

	@Override
	public Bodega save(Bodega bodega) {
		return bodegaRepository.save(bodega);
	}

	@Override
	public Bodega update(Bodega bodega, Integer id) {
		
		Bodega c = new Bodega();
		
		if(id != null && id > 0) {
			Optional<Bodega> co = bodegaRepository.findById(id);
			
			if(co.isPresent()) {
				bodega.setIdBodega(id);
				c = bodegaRepository.save(bodega);
			}else {
				throw new ModeloNotFoundException("La bodega no existe");
			}

		}else {
			throw new ModeloNotFoundException("Datos vacios");
		}
		
		return c;
		
	}

}
