package com.io.bodega.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.io.bodega.exceptions.ModeloNotFoundException;
import com.io.bodega.model.Marca;
import com.io.bodega.repository.MarcaRepository;
import com.io.bodega.service.MarcaService;

@Service
public class MarcaServiceImpl implements MarcaService {
	
	@Autowired
	private MarcaRepository marcaRepository;
	
	@Override
	public List<Marca> findAll() {
		return marcaRepository.findAll();
	}

	@Override
	public Marca findById(Integer id) {
		Optional<Marca> presentacionO = marcaRepository.findById(id);		
		if(presentacionO.isPresent()) {
			return presentacionO.get();
		}else {
			throw new ModeloNotFoundException("La marca no existe");
		}
	}

	@Override
	public Marca save(Marca presentacion) {
		return marcaRepository.save(presentacion);
	}
	
	@Override
	public Marca update(Marca marca, Integer id) {
		
		Marca c = new Marca();
		
		if(id != null && id > 0) {
			Optional<Marca> co = marcaRepository.findById(id);
			
			if(co.isPresent()) {
				marca.setIdMarca(id);
				c = marcaRepository.save(marca);
			}else {
				throw new ModeloNotFoundException("La marca no existe");
			}

		}else {
			throw new ModeloNotFoundException("Datos vacios");
		}
		
		return c;
		
	}

}
