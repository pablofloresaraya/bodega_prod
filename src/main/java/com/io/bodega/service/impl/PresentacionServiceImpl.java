package com.io.bodega.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.io.bodega.exceptions.ModeloNotFoundException;
import com.io.bodega.model.Presentacion;
import com.io.bodega.repository.PresentacionRepository;
import com.io.bodega.service.PresentacionService;

@Service
public class PresentacionServiceImpl implements PresentacionService {
	
	@Autowired
	private PresentacionRepository presentacionRepository;
	
	@Override
	public List<Presentacion> findAll() {
		return presentacionRepository.findAll();
	}
	
	@Override
	public Presentacion findById(Integer id) {
		Optional<Presentacion> presentacionO = presentacionRepository.findById(id);		
		if(presentacionO.isPresent()) {
			return presentacionO.get();
		}else {
			throw new ModeloNotFoundException("La presentacion no existe");
		}
	}
	
	@Override
	public Presentacion save(Presentacion presentacion) {
		return presentacionRepository.save(presentacion);
	}
	
	@Override
	public Presentacion update(Presentacion presentacion, Integer id) {
		
		Presentacion c = new Presentacion();
		
		if(id != null && id > 0) {
			Optional<Presentacion> co = presentacionRepository.findById(id);
			
			if(co.isPresent()) {
				presentacion.setIdPresentacion(id);
				c = presentacionRepository.save(presentacion);
			}else {
				throw new ModeloNotFoundException("La presentacion no existe");
			}

		}else {
			throw new ModeloNotFoundException("Datos vacios");
		}
		
		return c;
		
	}

}
