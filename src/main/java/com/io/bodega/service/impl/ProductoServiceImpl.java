package com.io.bodega.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.io.bodega.exceptions.ModeloNotFoundException;
import com.io.bodega.model.Producto;
import com.io.bodega.repository.ProductoRepository;
import com.io.bodega.repository.StockRepository;
import com.io.bodega.service.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService {
	
	@Autowired
	private ProductoRepository productoRepository;
	
	@Autowired
	private StockRepository stockRepository;
	
	@Override
	public List<Producto> findAll() {
		return productoRepository.findAll();
	}
	
	@Override
	public Producto findById(Integer id) {
		Optional<Producto> productoO = productoRepository.findById(id);		
		if(productoO.isPresent()) {
			return productoO.get();
		}else {
			throw new ModeloNotFoundException("El producto no existe");
		}
	}
	
	@Override
	public Producto save(Producto producto) {
		return productoRepository.save(producto);
	}
	
	@Override
	public Producto update(Producto producto, Integer id) {
		
		Producto c = new Producto();
		
		if(id != null && id > 0) {
			Optional<Producto> co = productoRepository.findById(id);
			
			if(co.isPresent()) {
				producto.setIdProducto(id);
				c = productoRepository.save(producto);
			}else {
				throw new ModeloNotFoundException("el registro no existe");
			}

		}else {
			throw new ModeloNotFoundException("Datos vacios");
		}
		
		return c;
		
	}
	

}
