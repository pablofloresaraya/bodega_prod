package com.io.bodega.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.io.bodega.exceptions.ModeloNotFoundException;
import com.io.bodega.model.Proveedor;
import com.io.bodega.repository.ProveedorRepository;
import com.io.bodega.service.ProveedorService;

@Service
public class ProveedorServiceImpl implements ProveedorService {
	
	@Autowired
	private ProveedorRepository proveedorRepository;
	
	@Override
	public List<Proveedor> findAll() {
		return proveedorRepository.findAll();
	}

	@Override
	public Proveedor findById(Integer id) {
		Optional<Proveedor> presentacionO = proveedorRepository.findById(id);		
		if(presentacionO.isPresent()) {
			return presentacionO.get();
		}else {
			throw new ModeloNotFoundException("El producto no existe");
		}
	}

	@Override
	public Proveedor save(Proveedor presentacion) {
		return proveedorRepository.save(presentacion);
	}

	@Override
	public Proveedor update(Proveedor presentacion, Integer id) {
		
		Proveedor c = new Proveedor();
		
		if(id != null && id > 0) {
			Optional<Proveedor> co = proveedorRepository.findById(id);
			
			if(co.isPresent()) {
				presentacion.setIdProveedor(id);
				c = proveedorRepository.save(presentacion);
			}else {
				throw new ModeloNotFoundException("La presentacion no existe");
			}

		}else {
			throw new ModeloNotFoundException("Datos vacios");
		}
		
		return c;
		
	}

}
