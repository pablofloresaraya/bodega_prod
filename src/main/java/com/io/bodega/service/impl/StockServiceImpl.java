package com.io.bodega.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.io.bodega.dto.StockDTO;
import com.io.bodega.model.Bodega;
import com.io.bodega.model.Producto;
import com.io.bodega.model.Stock;
import com.io.bodega.repository.StockRepository;
import com.io.bodega.service.StockService;

@Service
public class StockServiceImpl implements StockService {
	
	@Autowired
	private StockRepository stockRepository;
	
	@Override
	public List<Stock> findAll() {
		return stockRepository.findAll();
	}
	
	@Override
	public Stock save(Stock stock) {
		
		return stockRepository.save(stock);
		
	}
	

	

}
